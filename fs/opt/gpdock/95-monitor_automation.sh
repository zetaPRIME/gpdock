#!/usr/bin/env bash

# make sure xrandr gets the right xserver
export DISPLAY=:0.0
export XAUTHORITY=/home/zetaprime/.Xauthority

function use_external() {
  xrandr \
    --output DP1 \
      --primary \
      --auto \
      --set "Broadcast RGB" "Full" \
    --output eDP1 --off
  echo 0 | tee /sys/class/graphics/fbcon/rotate_all
}

function use_internal() {
  xrandr \
    --output eDP1 \
    --primary \
      --mode 720x1280 \
      --pos 0x0 \
      --rotate right \
      --set "Broadcast RGB" "Full" \
    --output DP1 --off
  echo 1 | tee /sys/class/graphics/fbcon/rotate_all
}

# select display by connection status of DP1
#xrandr | grep "DP1 disconnected" &> /dev/null && use_internal || use_external

S_DP1=$(cat /sys/class/drm/card0-DP-1/status)
if [ "$S_DP1" == "connected" ] ; then
  use_external &
elif [ "$S_DP1" == "disconnected" ] ; then
  use_internal &
fi

# show notification
export DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/1000/bus"
su zetaprime -c "notify-send \"External monitor $S_DP1\""

# wait for threads to close
wait
