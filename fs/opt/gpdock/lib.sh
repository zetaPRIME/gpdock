#!/usr/bin/env bash

# find info for given user
function get_user_info {
  _user_name="$(getent passwd $1 | cut -d: -f1)"
  _user_id="$(getent passwd $1 | cut -d: -f3)"
  _user_home="$(getent passwd $1 | cut -d: -f6)"
  export DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/$_user_id/bus"
}

# find current graphical session and set up relevant variables
function grab_session() {
  for s in `loginctl list-sessions --no-legend | awk '{print $1}'`; do
    export DISPLAY="$(loginctl show-session $s --value -p Display)"
    [ -z "$DISPLAY" ] && continue # skip if no attached display
    # TODO: maybe filter on Active=yes
    get_user_info "$(loginctl show-session $s --value -p User)"
    export XAUTHORITY=$_user_home/.Xauthority
    break # we're done here
  done
}

# run as user selected by get_user_info, in a way that systemctl --user works
function as_user {
  su - "$_user_name" --shell=/bin/bash -c "$*"
  return $?
}

# execute systemctl --user
function userctl {
  su - "$_user_name" --shell=/bin/bash -c "DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$_user_id/bus systemctl --user $*"
  return $?
}
  
# sends a notification to the active or specified user
function notify_user() {
  local msg="$2"
  if [ ! -z "$msg" ] ; then
    get_user_info $1
  else
    msg="$1"
  fi
  sudo -u "$_user_name" notify-send "$msg"
}

##### ### #####

# set up to use external monitor(s)
function set_mode_external() {
  xrandr \
    --output DP1 \
      --primary \
      --auto \
      --set "Broadcast RGB" "Full" \
    --output eDP1 --off
  # TODO: support daisy-chained displayport as second display
  #echo 0 | tee /sys/class/graphics/fbcon/rotate_all
  _display_mode="external"
}

# set up to use built-in LCD
function set_mode_internal() {
  xrandr \
    --output eDP1 \
    --primary \
      --mode 720x1280 \
      --pos 0x0 \
      --rotate right \
      --set "Broadcast RGB" "Full" \
    --output DP1 --off
  #echo 1 | tee /sys/class/graphics/fbcon/rotate_all
  _display_mode="internal"
}

# automatically set mode to match docked status
function set_mode_auto() {
  [ "$(cat /sys/class/drm/card0-DP-1/status)" == "connected" ] &&
    set_mode_external ||
    set_mode_internal
}

# set pulseaudio profile to match docked status
function set_pulseaudio() {
  [ "$(cat /sys/class/drm/card0-DP-1/status)" == "connected" ] &&
    local _out="hdmi-stereo-extra1" ||
    local _out="analog-stereo"
  sudo -u "$_user_name" pactl --server "unix:/run/user/$_user_id/pulse/native" set-card-profile 0 output:$_out+input:analog-stereo
}


















#
