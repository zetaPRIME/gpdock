#!/usr/bin/env bash
source /opt/gpdock/lib.sh

# grab graphical session context and set display mode
grab_session ; set_mode_auto
# switch pulseaudio to matching profile
set_pulseaudio
# and send a notification because we can
notify_user "Switching to $_display_mode display"
