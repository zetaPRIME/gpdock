#!/usr/bin/env bash

if [ ! -z "$USER" ]; then
    # fork self into transient systemd unit and stub current context
    systemd-run --setenv=_login_user=$USER /opt/gpdock/hook.lightdm-login.sh
    exit 0
fi

source /opt/gpdock/lib.sh

# fetch logging-in user's info
get_user_info "$_login_user"

# wait until systemd session exists for user
until userctl list-units _ > /dev/null ; do sleep 0.1 ; done

# abort if pulseaudio is entirely disabled or not installed
userctl is-enabled pulseaudio.socket > /dev/null ||
  userctl is-enabled pulseaudio.service > /dev/null ||
  exit 0

# make sure pulseaudio is started up
userctl start pulseaudio.service
# ...and set the proper profile
set_pulseaudio
