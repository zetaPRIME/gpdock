# GPDock

Set of scripts that automate switching between docked and undocked states on the GPD Win 2.

While docked, device automatically switches to using the external display(s) exclusively; this is intended to function as a miniature "desktop" that you can simply unplug and go somewhere with.

This currently only supports a single external display, using DisplayPort via the USB-C port; most hubs with HDMI use this.

Developed and tested on Arch Linux; other distros may have different conventions. Works best with lightdm, depends on systemd (I don't have anything with a non-systemd distro installed)
